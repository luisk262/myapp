<?php

namespace BackendBundle\Services;

use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class JWTAuth
{
    public $manager;
    public $key;
    public function __construct($manager)
    {
        $this->manager = $manager;
        $this->key="C1l2m3";
    }

    public function singup($email,$pass, $getHash=null)
    {
        $em= $this->manager;
        $user=$em->getRepository('BackendBundle:User')->findOneBy(array(
            "email"=>$email,
            "password"=>$pass
        ));
        $singup=false;
        if(is_object($user)){
            $singup=true;
        }
        if($singup==true){
            $token=array(
                "sub"=>$user->getId(),
                "email"=>$user->getEmail(),
                "name"=>$user->getName(),
                "iat"=>time(),
                "exp"=>time()+(7*24*60*60)
            );
            $jwt = JWT::encode($token,$this->key,'HS256');
            $decode=JWT::decode($jwt,$this->key,array('HS256'));
            if($getHash==null){
                $data=$jwt;
            }else{
                $data=$decode;
            }
        }else{
            $data=array(
                'status'=>'error',
                'data'=>'Fallo login'
            );
        }

        return $data;
    }
    public function checkToken($jwt ,$getIdentity = false){
        $auth=false;
        try{
            $decoded= JWT::decode($jwt,$this->key,array('HS256'));
        }
        catch (\UnexpectedValueException $e){
            $auth=false;
        }
        catch (\DomainException $e){
            $auth=false;
        }
        if(isset($decoded) && is_object($decoded) && isset($decoded->sub)){
            $auth=true;
        }
        else{
            $auth=false;
        }
        if( $getIdentity == false){
            return $auth;
        }else{
            return $decoded;
        }

    }
}
