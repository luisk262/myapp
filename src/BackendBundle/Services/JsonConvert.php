<?php

namespace BackendBundle\Services;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class JsonConvert{
    public function JsonConvert($data){
        $normalizers= array(new GetSetMethodNormalizer());
        $encoders= array("json"=> new JsonEncode());
        $serializer= new Serializer($normalizers,$encoders);
        $json= $serializer->serialize($data,'json');
        $response= new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type','application/json');
        return $response;
    }
}