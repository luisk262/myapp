<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Qualification
 *
 * @ORM\Table(name="qualification", indexes={@ORM\Index(name="Id_Song", columns={"id"}), @ORM\Index(name="Id_User", columns={"id"})})
 * @ORM\Entity
 */
class Qualification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Song
     *
     * @ORM\ManyToOne(targetEntity="Song")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Id_Song", referencedColumnName="id")
     * })
     */
    private $idSong;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User" ,cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Id_User", referencedColumnName="id")
     * })
     */
    private $idUser;
    /**
     * @var string
     *
     * @ORM\Column(name="qualification", type="string", length=2)
     */
    private $qualification;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qualification
     *
     * @param string $qualification
     *
     * @return Qualification
     */
    public function setQualification($qualification)
    {
        $this->qualification = $qualification;

        return $this;
    }

    /**
     * Get qualification
     *
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * Set idSong
     *
     * @param \BackendBundle\Entity\Song $idSong
     *
     * @return Qualification
     */
    public function setIdSong(\BackendBundle\Entity\Song $idSong = null)
    {
        $this->idSong = $idSong;

        return $this;
    }

    /**
     * Get idSong
     *
     * @return \BackendBundle\Entity\Song
     */
    public function getIdSong()
    {
        return $this->idSong;
    }

    /**
     * Set idUser
     *
     * @param \BackendBundle\Entity\User $idUser
     *
     * @return Qualification
     */
    public function setIdUser(\BackendBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \BackendBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
