<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Songlist
 *
 * @ORM\Table(name="list_song", indexes={@ORM\Index(name="Id_Song", columns={"id"}), @ORM\Index(name="Id_Lists", columns={"id"})})
 * @ORM\Entity
 */
class SongList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Song
     *
     * @ORM\ManyToOne(targetEntity="Song")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Id_Song", referencedColumnName="id")
     * })
     */
    private $idSong;

    /**
     * @var \List
     *
     * @ORM\ManyToOne(targetEntity="Lists" ,cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Id_List", referencedColumnName="id")
     * })
     */
    private $idList;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSong
     *
     * @param \BackendBundle\Entity\Song $idSong
     *
     * @return SongList
     */
    public function setIdSong(\BackendBundle\Entity\Song $idSong = null)
    {
        $this->idSong = $idSong;

        return $this;
    }

    /**
     * Get idSong
     *
     * @return \BackendBundle\Entity\Song
     */
    public function getIdSong()
    {
        return $this->idSong;
    }

    /**
     * Set idList
     *
     * @param \BackendBundle\Entity\Lists $idList
     *
     * @return SongList
     */
    public function setIdList(\BackendBundle\Entity\Lists $idList = null)
    {
        $this->idList = $idList;

        return $this;
    }

    /**
     * Get idList
     *
     * @return \BackendBundle\Entity\Lists
     */
    public function getIdList()
    {
        return $this->idList;
    }
}
