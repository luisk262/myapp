<?php

namespace BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use BackendBundle\Services\JWTAuth;

class DefaultController extends Controller
{
    /**
     * @Route("/login")
     * @Method({"POST"})
     */
    public function loginAction(Request $request)
    {
        $json = $request->get('json', null);
        $data = array(
            'status' => 'error',
            'data' => '404'
        );
        if ($json != null) {
            $parameters = json_decode($json);
            $email = (isset($parameters->email)) ? $parameters->email : null;
            $password = (isset($parameters->pass)) ? $parameters->pass : null;
            $getHash = (isset($parameters->getHash)) ? $parameters->getHash : null;
            $emailConstrain = new Assert\Email();
            $emailConstrain->message = "Verifique el email";
            $valite_Email = $this->get("validator")->validate($email, $emailConstrain);
            $pwd = hash('sha256', $password);
            if ($email != null && count($valite_Email) == 0 && $password != null) {
                $jwt_auth = $this->get("JWTAuth");
                if ($getHash == null || $getHash == false) {
                    $singup = $jwt_auth->singup($email, $pwd);
                } else {
                    $singup = $jwt_auth->singup($email, $pwd, true);
                }

                return $this->json($singup);
            } else {
                $data = array(
                    'status' => 'succsess',
                    'data' => 'Email o Contraseña incorrectos'
                );
            }
        }
        return $this->get('JsonConvert')->JsonConvert($data);
    }
}
