<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Song;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Song controller.
 *
 * @Route("song")
 */
class SongController extends Controller
{
    /**
     * Lists all song entities.
     *
     * @Route("/", name="song_index")
     * @Method("POST")
     */
    public function indexAction()
    {
        $JsonConvert=$this->get('JsonConvert');
        $em = $this->getDoctrine()->getManager();
        $songs = $em->getRepository('BackendBundle:Song')->findBy(array('disabled'=>0));
        $data=array(
            'status'=>'Success',
            'code'=>200,
            'data'=>$songs
        );
        return $JsonConvert->JsonConvert($data);
    }

    /**
     * Creates a new song entity.
     *
     * @Route("/new", name="song_new")
     * @Method({"POST"})
     */
    public function newAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $json=$request->get("json",null);
        $parameters=json_decode($json);
        $data=array(
            'status'=>'Error',
            'code'=>400,
            'msg'=>'Song not created'
        );
        if($json!=null){
            $createdAt= new  \DateTime('now');
            $name=(isset($parameters->name)? $parameters->name:null);
            $author=(isset($parameters->author)? $parameters->author:null);
            if($name != null && $author != null) {
                $em = $this->getDoctrine()->getManager();
                $exist_song=$em->getRepository('BackendBundle:Song')->findBy(array('name'=>$name,'author'=>$author));
                if(count($exist_song)==0){
                    $song = new Song();
                    $song->setName($name);
                    $song->setAuthor($author);
                    $song->setCreatedAt($createdAt);
                    $song->setDisabled(false);
                    $em->persist($song);
                    $em->flush();
                    $data = array(
                        'status' => 'Success',
                        'code' => 200,
                        'msg' => 'Song created!'
                    );
                }else{
                    $data=array(
                        'status'=>'Error',
                        'code'=>400,
                        'msg'=>'Song Exist!'
                    );
                }
            }else{
                $data=array(
                    'status'=>'Error',
                    'code'=>400,
                    'msg'=>'Check fields!'
                );
            }
        }
        return $JsonConvert->JsonConvert($data);
    }

    /**
     * Finds and displays a song entity.
     *
     * @Route("/show/{id}", name="song_show")
     * @Method("POST")
     */
    public function showAction(Song $song)
    {
        $JsonConvert=$this->get('JsonConvert');
        if(is_object($song)){
            $data=array(
                'status'=>'Success',
                'code'=>200,
                'data'=>$song
            );
        }else{
            $data=array(
                'status'=>'Error',
                'code'=>404,
                'msg'=>'not found'
            );
        }
        return $JsonConvert->JsonConvert($data);
    }

    /**
     * Displays a form to edit an existing song entity.
     *
     * @Route("/edit", name="song_edit")
     * @Method({"POST"})
     */
    public function editAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $json=$request->get("json",null);
        $parameters=json_decode($json);
        $idSong=(isset($parameters->id)? $parameters->id:null);
        $name=(isset($parameters->name)? $parameters->name:null);
        $author=(isset($parameters->author)? $parameters->author:null);
        $em=$this->getDoctrine()->getManager();
        $song = $em->getRepository('BackendBundle:Song')->find(intval($idSong));
        if(is_object($song)){
            if($song->getName()!=$name && $name!=null)$song->setName($name);
            if($song->getAuthor()!=$author && $author!=null)$song->setAuthor($author);
            $em = $this->getDoctrine()->getManager();
            $em->persist($song);
            $em->flush();
            $data = array(
                'status' => 'Success',
                'code' => 200,
                'msg' => 'Updated song!'
            );
        }else{
            $data=array(
                'status'=>'Error',
                'code'=>404,
                'msg'=>'not found'
            );
        }
        return $JsonConvert->JsonConvert($data);
    }

    /**
     * Deletes a song entity.
     *
     * @Route("/delete", name="song_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $json=$request->get("json",null);
        $parameters=json_decode($json);
        $idSong=(isset($parameters->id)? $parameters->id:null);
        $em = $this->getDoctrine()->getManager();
        $song=$em->getRepository('BackendBundle:Song')->find($idSong);
        $data=array(
            'status'=>'error',
            'code'=>400,
            'msg'=>'Song not found'
        );
        if(is_object($song)){
            $song->setDisabled(true);
            $em->persist($song);
            $em->flush();
            $data=array(
                'status'=>'success',
                'code'=>200,
                'msg'=>'Deleted'
            );
        }

        return $JsonConvert->JsonConvert($data);
    }
}
