<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        /*
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('BackendBundle:User')->findAll();
        */
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $json=$request->get("json",null);
        $parameters=json_decode($json);
        $data=array(
            'status'=>'error',
            'code'=>400,
            'msg'=>'User not created'
        );
        if($json!=null){
            $createdAt= new  \DateTime('now');
            $role="user";
            $email=(isset($parameters->email)? $parameters->email:null);
            $pass=(isset($parameters->pass)? $parameters->pass:null);
            $name=(isset($parameters->name)? $parameters->name:null);

            $emailContraint =  new Assert\Email();
            $emailContraint->message ="Verifique el email";
            $validate_email= $this->get('validator')->validate($email,$emailContraint);
            if($email != null && count($validate_email) == 0 && $pass != null && $name != null){
                $user = new User();
                $em = $this->getDoctrine()->getManager();
                $user_exist=$em->getRepository('BackendBundle:User')->findBy(array('email'=>$email));
                if(count($user_exist)==0){
                    $user->setEmail($email);
                    $user->setName($name);
                    $user->setRole($role);
                    $user->setCreatedAt($createdAt);
                    $pwd = hash('sha256',$pass);
                    $user->setPassword($pwd);
                    $em->persist($user);
                    $em->flush();

                    $correo_remitente = 'slist2017@gmail.com';
                    $data['titulo1']='Bienvenido';
                    $data['body']="Bienvenido, Puede administrar sus listas xD.";
                    $email = $user->getEmail();
                    try{
                        $message = \Swift_Message::newInstance()
                            ->setSubject($data['titulo1'])
                            ->setFrom($correo_remitente)
                            ->setTo($email)
                            ->setBody($data['body'],'text/html')
                        ;
                        $this->get('mailer')->send($message);
                    }catch (\Swift_SwiftException $e){
                    }
                    $data=array(
                        'status'=>'success',
                        'code'=>200,
                        'msg'=>'User created!'
                    );
                }else{
                    $data=array(
                        'status'=>'error',
                        'code'=>400,
                        'msg'=>'User exist!'
                    );
                }

            }
        }
        return $JsonConvert->JsonConvert($data);
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {

    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {

    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
    }

}
