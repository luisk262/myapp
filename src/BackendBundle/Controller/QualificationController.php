<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Qualification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Qualification controller.
 *
 * @Route("qualification")
 */
class QualificationController extends Controller
{
    /**
     * Lists all list entities.
     *
     * @Route("/", name="qualification_index")
     * @Method("POST")
     */
    public function indexAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $token=$request->get("authorization",null);
        $em = $this->getDoctrine()->getManager();
        $identity=$this->get('JWTAuth')->checkToken($token,true);
        $user_log=$em->getRepository('BackendBundle:User')->findOneBy(array('email'=>$identity->email,'name'=>$identity->name));
        $qualifications = $em->getRepository('BackendBundle:Qualification')->findBy(array('idUser'=>$user_log));
        if(count($qualifications)>0){
            $data=array(
                'status'=>'Success',
                'code'=>200,
                'data'=>$qualifications
            );
        }else{
            $data=array(
                'status'=>'Success',
                'code'=>200,
                'msg'=>'not qualifications for user'
            );
        }

        return $JsonConvert->JsonConvert($data);
    }
    /**
     * Creates a new qualification entity.
     *
     * @Route("/new", name="qualification_new")
     * @Method({"POST"})
     */
    public function newAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $json=$request->get("json",null);
        $token=$request->get("authorization",null);
        $data=array(
            'status'=>'Error',
            'code'=>400,
            'msg'=>'Qualification not created'
        );
        if($token && $this->get('JWTAuth')->checkToken($token)){
            if($json != null){
                $parameters=json_decode($json);
                $score = (isset($parameters->score)? $parameters->score:null);
                $idsong = (isset($parameters->song)? $parameters->song:null);
                if($score != null && $idsong != null){
                    $qualification = new Qualification();
                    $em = $this->getDoctrine()->getManager();
                    $identity=$this->get('JWTAuth')->checkToken($token,true);
                    $user_log=$em->getRepository('BackendBundle:User')->findOneBy(array('email'=>$identity->email,'name'=>$identity->name));
                    $song = $em->getRepository('BackendBundle:Song')->find($idsong);
                    $qualification_exist=$em->getRepository('BackendBundle:Qualification')->findBy(array('idSong'=>$idsong,'idUser'=>$user_log));
                    if(is_object($song) && (count($qualification_exist)==0)){
                        $qualification->setIdSong($song);
                        $qualification->setIdUser($user_log);
                        $qualification->setQualification($score);
                        $em->persist($qualification);
                        $em->flush();
                        $data=array(
                            'status'=>'Success',
                            'code'=>200,
                            'data'=>'qualification created'
                        );
                    }
                }else{
                    $data=array(
                        'status'=>'Error',
                        'code'=>400,
                        'data'=>'Check parameters'
                    );
                }
            }
        }else{
            $data=array(
                'status'=>'Error',
                'code'=>400,
                'msg'=>'Invalid authorization'
            );
        }

        return $JsonConvert->JsonConvert($data);
    }
    /**
     * Displays a form to edit an existing qualification entity.
     *
     * @Route("/{id}/edit", name="qualification_edit")
     * @Method({"POST"})
     */
    public function editAction(Request $request, Qualification $qualification)
    {
    }

    /**
     * Deletes a qualification entity.
     *
     * @Route("/{id}", name="qualification_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Qualification $qualification)
    {
    }
}
