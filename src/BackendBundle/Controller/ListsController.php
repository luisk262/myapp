<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Lists;
use BackendBundle\Entity\Song;
use BackendBundle\Entity\SongList;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * List controller.
 *
 * @Route("lists")
 */
class ListsController extends Controller
{
    /**
     * Lists all list entities.
     *
     * @Route("/", name="lists_index")
     * @Method("POST")
     */
    public function indexAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $token=$request->get("authorization",null);
        $em = $this->getDoctrine()->getManager();
        $identity=$this->get('JWTAuth')->checkToken($token,true);
        $user_log=$em->getRepository('BackendBundle:User')->findOneBy(array('email'=>$identity->email,'name'=>$identity->name));
        $songs = $em->getRepository('BackendBundle:Lists')->findBy(array('disabled'=>0,'idUser'=>$user_log));
        $data=array(
            'status'=>'Success',
            'code'=>200,
            'data'=>$songs
        );
        return $JsonConvert->JsonConvert($data);
    }

    /**
     * Creates a new list entity.
     *
     * @Route("/new", name="lists_new")
     * @Method({"POST"})
     */
    public function newAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $token=$request->get("authorization",null);
        $json=$request->get("json",null);
        $parameters=json_decode($json);
        $data=array(
            'status'=>'Error',
            'code'=>400,
            'msg'=>'List not created'
        );
        if($token && $this->get('JWTAuth')->checkToken($token)){
            if($json!=null){
                $createdAt= new  \DateTime('now');
                $name=(isset($parameters->name)? $parameters->name:null);
                if($name != null) {
                    $list = new Lists();
                    $em =$this->getDoctrine()->getManager();
                    $identity=$this->get('JWTAuth')->checkToken($token,true);
                    $user_log=$em->getRepository('BackendBundle:User')->findOneBy(array('email'=>$identity->email,'name'=>$identity->name));
                    if(count($em->getRepository('BackendBundle:Lists')->findBy(array('name'=>$name,'idUser'=>$user_log)))>0){
                        $data=array(
                            'status'=>'Error',
                            'code'=>400,
                            'msg'=>'List Exist'
                        );
                    }else{
                        if(!empty($user_log)){
                            $list->setName($name);
                            $list->setDisabled(false);
                            $list->setIdUser($user_log);
                            $list->setCreatedAt($createdAt);
                            $em->persist($list);
                            $em->flush();
                            $data=array(
                                'status'=>'Success',
                                'code'=>200,
                                'msg'=>'list created'
                            );
                        }else{
                            $data=array(
                                'status'=>'Error',
                                'code'=>400,
                                'msg'=>'Invalid authorization'
                            );
                        }
                    }
                }
            }
        }else{
            $data=array(
                'status'=>'Error',
                'code'=>400,
                'msg'=>'Invalid authorization'
            );
        }
        return $JsonConvert->JsonConvert($data);
    }

    /**
     * Finds and displays a list entity.
     *
     * @Route("/show", name="lists_show")
     * @Method("POST")
     */
    public function showAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $json=$request->get("json",null);
        $parameters=json_decode($json);

        $idList=(isset($parameters->id)? $parameters->id:null);
        $em = $this->getDoctrine()->getManager();
        $songsList=$em->getRepository('BackendBundle:SongList')->findBy(array('idList'=>$idList));
        if(count($songsList)>0){
            $songs=array();
            foreach ($songsList as $songlist ){
                array_push($songs,$songlist->getIdSong());
            }
            $data=array(
                'status'=>'Success',
                'code'=>200,
                'data'=>$songs
            );
        }else{
            $data=array(
                'status'=>'Success',
                'code'=>200,
                'msg'=>'not songs'
            );
        }
        return $JsonConvert->JsonConvert($data);
    }

    /**
     * Displays a form to edit an existing list entity.
     *
     * @Route("/edit", name="lists_edit")
     * @Method({"POST"})
     */
    public function editAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $json=$request->get("json",null);
        $token=$request->get("authorization",null);
        $parameters=json_decode($json);

        if($token && $this->get('JWTAuth')->checkToken($token)){
            $idList=(isset($parameters->id)? $parameters->id:null);
            $name=(isset($parameters->name)? $parameters->name:null);
            $em = $this->getDoctrine()->getManager();
            $list = $em->getRepository('BackendBundle:Lists')->find($idList);
            if(is_object($list)){
                $identity=$this->get('JWTAuth')->checkToken($token,true);
                $user_log=$em->getRepository('BackendBundle:User')->findOneBy(array('email'=>$identity->email,'name'=>$identity->name));
                if($user_log->getId()==$list->getIdUser()->getId()){
                    if($list->getName()!=$name && $name!=null){
                        $list->setName($name);
                        $em->persist($list);
                        $em->flush();
                        $data = array(
                            'status' => 'Success',
                            'code' => 200,
                            'msg' => 'Updated list!'
                        );
                    }else{
                        $data=array(
                            'status'=>'Error',
                            'code'=>400,
                            'msg'=>'Not updated!'
                        );
                    }

                }else{
                    $data=array(
                        'status'=>'Error',
                        'code'=>400,
                        'msg'=>'Invalid authorization'
                    );
                }
            }else{
                $data=array(
                    'status'=>'Error',
                    'code'=>404,
                    'msg'=>'not found'
                );
            }
        }else{
            $data=array(
                'status'=>'Error',
                'code'=>400,
                'msg'=>'Invalid authorization'
            );
        }

        return $JsonConvert->JsonConvert($data);

    }

    /**
     * Deletes a list entity.
     *
     * @Route("/delete", name="lists_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $json=$request->get("json",null);
        $parameters=json_decode($json);
        $idList=(isset($parameters->id)? $parameters->id:null);
        $em = $this->getDoctrine()->getManager();
        $list=$em->getRepository('BackendBundle:Lists')->find($idList);

        $list->setDisabled(true);
        $em->persist($list);
        $em->flush();
        $data=array(
            'status'=>'success',
            'code'=>200,
            'msg'=>'Deleted'
        );
        return $JsonConvert->JsonConvert($data);
    }
    /**
     * Finds and displays a list entity.
     *
     * @Route("/{id}/{idSong}/add/song", name="lists_add_song")
     * @Method("POST")
     */
    public function addsongAction($id,$idSong,Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $em=$this->getDoctrine()->getManager();
        $token=$request->get("authorization",null);
        if($token && $this->get('JWTAuth')->checkToken($token)){
            $identity=$this->get('JWTAuth')->checkToken($token,true);
            $user_log=$em->getRepository('BackendBundle:User')->findOneBy(array('email'=>$identity->email,'name'=>$identity->name));
            $list=$em->getRepository('BackendBundle:Lists')->findOneBy(array('id'=>$id,'idUser'=>$user_log));
            $song=$em->getRepository('BackendBundle:Song')->find($idSong);
            if(is_object($list) && is_object($song)){
                $SongList= new SongList();
                $SongList->setIdList($list);
                $SongList->setIdSong($song);
                $em->persist($SongList);
                $em->flush();
                $data=array(
                    'status'=>'Success',
                    'code'=>200,
                    'data'=>$list
                );
            }else{
                $data=array(
                    'status'=>'Error',
                    'code'=>404,
                    'msg'=>'not found'
                );
            }
        }else{
            $data=array(
                'status'=>'Error',
                'code'=>404,
                'msg'=>'Invalid authorization'
            );
        }
        return $JsonConvert->JsonConvert($data);
    }
    /**
     * Finds and displays a list entity.
     *
     * @Route("/{id}/{idlist}/SongList/remove", name="lists_remove_song")
     * @Method("POST")
     */
    public function removesongAction($id,$idlist,Request $request)
    {
        $JsonConvert=$this->get('JsonConvert');
        $em=$this->getDoctrine()->getManager();
        $token=$request->get("authorization",null);
        if($token && $this->get('JWTAuth')->checkToken($token)){
            $identity=$this->get('JWTAuth')->checkToken($token,true);
            $user_log=$em->getRepository('BackendBundle:User')->findOneBy(array('email'=>$identity->email,'name'=>$identity->name));
            $SongList=$em->getRepository('BackendBundle:SongList')->findOneBy(array('idSong'=>$id,'idList'=>$idlist));
            if (is_object($SongList)){
                $list=$em->getRepository('BackendBundle:Lists')->findOneBy(array('id'=>$SongList->getIdList(),'idUser'=>$user_log));
                if(is_object($list)){
                    $em->remove($SongList);
                    $em->flush();
                    $data=array(
                        'status'=>'Success',
                        'code'=>200,
                        'msg'=>'Deleted'
                    );
                }else{
                    $data=array(
                        'status'=>'Error',
                        'code'=>404,
                        'msg'=>'not found'
                    );
                }
            }else{
                $data=array(
                    'status'=>'Error',
                    'code'=>404,
                    'msg'=>'not found'
                );
            }
        }else{
            $data=array(
                'status'=>'Error',
                'code'=>404,
                'msg'=>'Invalid authorization'
            );
        }
        return $JsonConvert->JsonConvert($data);
    }
}
